import { useEffect, useState } from 'react';
import {
  Container,
  Typography,
  Paper,
  Box,
  withTheme
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  Container: { padding: 10 },
  TooHigh: { backgroundColor: 'purple', color: 'white', fontWeight: 'bold', padding: 3 },
  TooLow: { backgroundColor: 'red', color: 'white', fontWeight: 'bold', padding: 3 },
  Ok: { backgroundColor: 'green', color: 'white', fontWeight: 'bold', padding: 3 },
});

const data = [
  {
    id: '1',
    name: 'Pilsner',
    minimumTemperature: 4,
    maximumTemperature: 6,
  },
  {
    id: '2',
    name: 'IPA',
    minimumTemperature: 5,
    maximumTemperature: 6,
  },
  {
    id: '3',
    name: 'Lager',
    minimumTemperature: 4,
    maximumTemperature: 7,
  },
  {
    id: '4',
    name: 'Stout',
    minimumTemperature: 6,
    maximumTemperature: 8,
  },
  {
    id: '5',
    name: 'Wheat beer',
    minimumTemperature: 3,
    maximumTemperature: 5,
  },
  {
    id: '6',
    name: 'Pale Ale',
    minimumTemperature: 4,
    maximumTemperature: 6,
  },
];

function App() {
  const classes = useStyles();
  const [items, setItems] = useState({});

  useEffect(() => {
    const request = () =>
      data.forEach((product) => {
        fetch(`http://localhost:8081/temperature/${product.id}`)
          .then((response) => response.json())
          .then((response) =>
            setItems((prevItems) => ({
              ...prevItems,
              [product.id]: {
                ...product,
                ...response,
              },
            }))
          );
      });

    setInterval(request, 5000);

    request();
  }, []);

  return (
    <div className="App">
      <Container>
        <Paper className={classes.Container}>
          <Box>
            <Typography variant="h3" component="h2" gutterBottom>
              SensorTech
            </Typography>
          </Box>
          <Box>
            <h2>Beers</h2>
          </Box>
          <Box>
            <Box display="flex" flexDirection="row">
              <Box flexGrow={1}><h2>Product</h2></Box>
              <Box flexGrow={1}><h2>Temperature</h2></Box>
              <Box flexGrow={1}><h2>Status</h2></Box>
            </Box>
            {Object.keys(items).map((itemKey) => (
              <Box key={items[itemKey].id} display="flex" flexDirection="row">
                <Box flexGrow={1} width={250} height={30}>{items[itemKey].name}</Box>
                <Box flexGrow={1} width={280} height={30}>{items[itemKey].temperature}</Box>
                <Box flexGrow={1} width={250} height={30}>
                  {items[itemKey].temperature <
                    items[itemKey].minimumTemperature && <span className={classes.TooLow}>too low</span>}
                  {items[itemKey].temperature >
                    items[itemKey].maximumTemperature && <span className={classes.TooHigh}>too high</span>}
                  {items[itemKey].temperature <=
                    items[itemKey].maximumTemperature &&
                    items[itemKey].temperature >=
                      items[itemKey].minimumTemperature && <span className={classes.Ok}>all good</span>}
                </Box>
              </Box>
            ))}
          </Box>
        </Paper>
      </Container>
    </div>
  );
}

export default App;
