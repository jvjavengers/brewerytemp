const express = require('express');
const fetch = require('node-fetch');
// const helmet = require('helmet')
const cors = require('cors');

const app = express();
const port = 8081;

// app.use(helmet())
app.use(cors());

app.get('/temperature/:id', (req, res) => {
    fetch(
      `https://temperature-sensor-service.herokuapp.com/sensor/${req.params.id}`
    )
      .then((response) => response.json())
      .then((response) => res.send(response)).catch((err) => {
        console.log('Unable to consume api')
        res.json(err)
      })
});

app.listen(port, () => {
  console.log(`SensorTech server at http://localhost:${port}`);
});
