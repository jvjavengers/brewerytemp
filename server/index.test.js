const { expectCt } = require('helmet');
const fetch = require('node-fetch');

describe('Risk Coverage', () => {
  it('Should return ENOTFOUND when api unreacheable, else call should be successful', async () => {
    const result = await fetch(
      `http://localhost:8081/temperature/1`
    )
    const obj = await result.json()
    expect(obj.errno === 'ENOTFOUND' || !obj.errno).toBeTruthy()
  })

  it('Should return true when api is available', async () => {
    const result = await fetch(
      `http://localhost:8081/temperature/1`
    )
    const obj = await result.json()
    const value = Number.isInteger(obj.temperature)
    
    expect(value).toBeTruthy()
  })
})