# README #
By: Jesse Junsay
LinkedIn: https://www.linkedin.com/in/jesse-junsay-74238214/


### What is this repository for? ###

This is a code challenge regarding a simple automated brewery temperature monitoring system.

### Setup ###

1. clone the code
2. run npm i in the following folders
   - root
   - client
   - server
3. run by going to root and type "npm start"

For testing
1. go to server folder
2. run "npm run test"

### Requirements ###

1. Increase Test Coverage for the system

### Approach ###

1. Review the requirements and business needs
2. Review the existing code
3. Review test coverage

### TDD Approach ###

1. Recognized the needed test coverage
   a. Product Coverage
      - Is the UI simple to understand for the intended user
      - Are the data presented intuiatively
	    - Are the data in realtime
   b. Risk Coverage
      - Does the application able to manage when the api server unreachable
	    - Is it safe from spaming
   c. Requirement Coverage
      - Realtime monitoring of different types of beer containers in their truck
	    - Notify them if the temperature goes out of range
   d. Code Coverage
      - Does the client app able to handle any communication issue between the server
	    - Does the server app able to handle any communication issue between the api
	    - Does the api returns a valid data
	    - Does the client show correct results

2. Explanation
   Above is my strategy to expand test coverage. Starting with Product Coverage down to code coverage.
   There are other techniques, but I think these covers  all the requirements enough at the moment.

   If I have more time would like to continue with other test coverage. And improve security and logging of the backend.
   By adding parameter checking, logger, and improve error handler.

   For the UI, tried improving the looks, not too fancy. Just to keep it simple, functional, and fast. I added colored labels 
   in the status so users can see the status immediately without reading the whole word. Red for low temerature, purple for high,
   and green for good.

3. Questions
   a. Will the client require a history of the data
   b. Would they want this on mobile device too? Tablets?
